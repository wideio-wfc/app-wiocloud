# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################

#!/usr/bin/env python
# -*- encoding:utf-8 -*-

import os
import datetime
#from django.db import models
import wioframework.fields as models
from django.core.urlresolvers import reverse

from django.contrib.auth.models import AbstractUser  # , User
import zipfile
import settings
import json

from accounts.models import UserAccount

from wioframework.amodels import wideiomodel, wideio_owned, get_dependent_models, wideio_timestamped, wideio_action
from wioframework import decorators as dec

if False:
    @wideiomodel
    @wideio_timestamped
    class ComputeCloudNode(models.Model):

        """
        (theoretical computer node) fields to be made uniform with ComputerNode
        """
        name = models.TextField()
        provider = models.ForeignKey('cloud.CloudProvider')
        ###
        os_family = models.CharField(max_length=3)
        os_version = models.CharField(max_length=12)
        cpu_family = models.CharField(max_length=6)
        cpu_spec = models.TextField()
        ram = models.IntegerField()
        cachesize = models.IntegerField(verbose_name="Cache size")
        iospeed = models.IntegerField()  # bytes per second
        # location for load balancing
        region = models.CharField(max_length=3)
        location = models.CharField(max_length=3)
        # software characteristics
        runtimeimageversion = models.ForeignKey(
            'software.SoftwareVersion',
            verbose_name="Version")
        # pricing=models.ForeignKey(PricingPolicy) # TODO / FIXME
        # current status
        loadavg = models.IntegerField()  # fix precision /1000
        remaining_free_hd = models.IntegerField()  # megabytes

        def __unicode__(self):
            return self.name

        class WIDEIO_Meta:
            icon = "icon-circle"
            permissions = dec.perm_for_admin_only
            views_kwargs = {"list_add_enabled": False}
            NO_MOCKS = True