#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-

import wioframework.fields as models
from wioframework.amodels import wideiomodel, wideio_timestamped
from wioframework import decorators as dec
from extfields.fields import *


@wideiomodel
@wideio_timestamped
class NodeType(models.Model):
    """
    Specification a compute node.

    This can be used for abitrary node type specifications.
     must have to be able to run a package
    """
    name = models.CharField(max_length=128)
    os_family = models.CharField(max_length=3)
    os_version = models.CharField(max_length=12)
    cpu_family = models.CharField(max_length=6)
    cpu_spec = models.TextField()
    ram = models.IntegerField()
    cachesize = models.IntegerField(verbose_name="Cache size")
    iospeed = models.IntegerField(verbose_name="IO speed")  # bytes per second
    # --------------------------------------------------------------------
    pricing_model = models.ForeignKey('cloud.NodePricingModel')
    metadata = JSONField()  # < Other metadata

    def __unicode__(self):
        return self.os_family + self.os_version + self.cpu_family

    class WIDEIO_Meta:
        sort_enabled = ["ram", "created_at"]
        search_enabled = "name"
        icon = "icon-laptop"
        permissions = dec.perm_for_staff_only

        MOCKS = {
            'default': [
                {
                    '@id': 'cloud-nodetype-0000',
                    'name': 'Metal Server',
                    'os_family': 'N/A',
                    'os_version': 'N/A',
                    'cpu_family': 'Intel',
                    'cpu_spec': '4Ghz 16-core',
                    'ram': 8 * 1024 * 1024 * 1024,
                    'cachesize': 0,
                    'iospeed': 0,
                    'pricing_model': 'cloud-nodepricing-0000',
                    'metadata': {}
                }
            ]
        }
