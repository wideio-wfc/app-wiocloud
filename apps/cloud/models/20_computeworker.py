#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-

import datetime
import wioframework.fields as models

from accounts.models import UserAccount
from wioframework.amodels import wideiomodel,wideio_timestamped, wideio_action
from wioframework import decorators as dec

@wideio_timestamped
@wideiomodel
class Worker(models.Model):
    """
    This is a process run for a user of our network
    """
    node=models.ForeignKey("cloud.Node",db_index=True,related_name="workers")
    wid=models.CharField(max_length=16)
    
    used_by=models.ForeignKey(UserAccount,null=True,db_index=True,related_name="workers") # < SYSTEM PROCESS MUST LINK TO ADMIN/SYSTEM ACCOUNT    
    used_since=models.DateTimeField(db_index=True)
    used_credits=models.FloatField(default=0)
    last_update_used_credits=models.DateTimeField(default=lambda:datetime.datetime.now())
    credit_reserve=models.FloatField(null=True,db_index=True) #< reserved for use to avoid to check permanently the main credit account
    auto_topup=models.BooleanField(default=True,db_index=True)

    can_access_network=models.BooleanField(default=True,db_index=True)
    memory_limit=models.IntegerField(null=True,db_index=True)
    cpu_limit=models.FloatField(null=True,db_index=True)    
    credit_limit=models.FloatField(null=True,db_index=True)
    base_distribution=models.ForeignKey('compute.BaseDistribution',null=True,db_index=True)

    def __unicode__(self):
        return u"%s - %r"%(self.node, self.wid)
    
    def status(self):
        if self.used_by!=None:
            return "in use"
        else:
            return "idle"
        
    def get_associated_request(self):
        if self.metadata:
            if "request" in self.metadata:
                if "schedreqrep_id" in self.metadata["request"]:
                    from compute.models import SchedReqRep
                    return SchedReqRep.objects.get(id=self.metadata["request"]["schedreqrep_id"])
        return None
    
    @staticmethod
    def can_add(request):
        return False
    
    def can_delete(self,request):
        return False    

    def can_update(self, request):
        return ((self.used_by==request.user) or request.user.is_staff) and (request.GET["_KEYS"]!=None) and reduce(lambda c,o:c and ("_limit" in o),request.GET["_KEYS"].split(","), True)

    def can_view(self,request):
        return ((self.used_by==request.user) or request.user.is_staff) 

    
    def set_basedistribution(self,new_basedistribution):
        self.base_disribution=new_base_distribution
        if hasattr(new_basedistribution,"id"):
            new_basedistribution=new_basedistribution.id
        self.node.send_reqrep(request.user,{'opcode':'set_basedistribution','wid':self.wid,'basedistribution':new_basedistribution})        
        return 'alert("ok");'
    
    
    def enumerate_compatible_distributions(self):
        return self.node.enumerate_compatible_distributions()
        
    def load_environment(self,environment):
        # TODO: IMPLEMENT
        return 'alert("NYI");'
    
    class WIDEIO_Meta:
        NO_DRAFT = True
        DISABLE_VIEW_ACCOUNTING = True
        icon = "icon-circle"
        permissions =  dec.perm_for_logged_users_only

        class Actions:
            @wideio_action(possible=lambda o,r:((r.user==o.used_by) or (request.user.is_staff)),icon="icon-skull")
            def kill_current_task(self,request):
               self.node.send_reqrep(request.user,{'opcode':'kill','wid':self.wid})
               return 'alert("ok");'
