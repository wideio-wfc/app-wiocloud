#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-

import wioframework.fields as models

from wioframework.amodels import wideiomodel, wideio_owned
from wioframework import decorators as dec


@wideio_owned()
@wideiomodel
class BucketAlloc(models.Model):

    """
    This is a virtual node that is part of our network
    """
    node = models.ForeignKey('cloud.Node')
    bucketid = models.CharField(max_length=64)
    wrapped_access_key = models.CharField(max_length=128)

    required_memory = models.IntegerField()  # required memory in Mb
    persistent = models.BooleanField(default=False)
    request = models.ForeignKey('compute.ExecRequest')

    @staticmethod
    def can_add(self):
        return False

    def list_content(self):
        return ["not_yet_implemented"]

    class WIDEIO_Meta:
        NO_DRAFT = True
        icon = "icon-foder"
        permissions = dec.perm_for_admin_only
        MOCKS = {
            'default': [
                {
                'node' : 'cloud-node-0000',
                'bucketid': 'compute-bucket-0000',
                'wrapped_access_key': '0123131910419841089014',

                'required_memory': 2*1024*1024*1024,
                'persistent': True,
                'request': 'compute-exec-request-0000'
                }
            ]
        }