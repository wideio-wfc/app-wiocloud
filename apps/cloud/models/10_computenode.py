#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-

import os
import datetime
import wioframework.fields as models
from django.db import transaction
from django.http import HttpResponseRedirect
import uuid
from wioframework.amodels import wideiomodel, wideio_action, wideio_history_track, wideio_relaction
from wioframework import decorators as dec
import socket


def get_local_df(ignore=["/proc", "/sys", "/dev"]):
    pid = os.getpid()
    mounts = open("/proc/%d/mounts" % (pid,), "rb").readlines()
    r = {}
    for me in mounts:
        me = me.split(" ")
        d = me[1]
        skip = False
        for i in ignore:
            if d.startswith(i):
                skip = True
                break
        if not skip:
            try:
                s = os.statvfs(d)
                r[d] = {
                    's': s.f_bsize * s.f_bfree / (1024 * 1024.0), 'f': s.f_ffree}
            except:
                pass
    return r


def get_local_sshkey():
    local_sshkey = os.path.expanduser("~/.ssh/id_rsa")
    if (not (os.path.exists(local_sshkey + ".pub"))):
        os.system("ssh-keygen -q -t rsa -b 4096 -p -N '' -f " + local_sshkey)
    return file(local_sshkey + ".pub").read()


def get_local_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


def get_cpu_count():
    import multiprocessing
    return multiprocessing.cpu_count()


def meminfo():
    mi = dict()
    for line in file('/proc/meminfo', "r").readlines():
        mi[line.split(':')[0]] = line.split(':')[1].strip()
    return mi


def uptime():
    return file('/proc/uptime', "r").read().split(" ")


@wideio_history_track(["system_load",
                       "available_memory",
                       "available_disk",
                       "available_bucket_storage"],
                      length=datetime.timedelta(1),
                      max_update_interval=500,
                      history_suffix="_longhist")
@wideio_history_track(["system_load",
                       "available_memory",
                       "available_disk",
                       "available_bucket_storage"],
                      length=1800,
                      max_update_interval=30)
@wideiomodel
class Node(models.Model):
    """
    This is a virtual node that is part of our network.

    This node can be implemented in different way. The important
    point being that it must replies to some of the basic services that we expect
    our services to reply to and to follow the right logic.
    """
    ##
    ## HOST ID AND NETWORK
    ##
    hostname = models.CharField(max_length=64, default=lambda: socket.gethostname())
    hostip = models.CharField(max_length=16, default=get_local_ip)
    basetype = models.CharField(
        max_length=16,
        choices=[
            ("co", "compute node"),
            ("we", "web server"),
            ("db", "database node"),
            ("st", "storage node"),
        ],
        default="we")
    hosttype = models.CharField(max_length=64)

    ##
    ## AUTHENTICATIO & IDENTIFICATION
    ##
    pub_key_ssh = models.CharField(
        max_length=4096,
        default=get_local_sshkey)  # USED TO ALLOW THIS HOST TO ACCESS OTHER RESOURCES

    ##
    ## HARDWARE INFO
    ##
    # NCORES
    cpu_count = models.IntegerField()
    # MEM INFO
    total_memory = models.IntegerField()  # in mb
    available_memory = models.IntegerField()  # in mb
    # available_bucket_disk=models.IntegerField()  # in mb
    # WORKERS FOR COMPUTE
    nworkers = models.IntegerField(default=-1)
    # USAGE
    system_load = models.CharField(max_length=64)
    started_at = models.DateTimeField()
    available_disk = models.TextField(max_length=512)
    available_bucket_storage = models.IntegerField(default=0)
    last_ping = models.DateTimeField()
    # location for load balancing
    region = models.CharField(max_length=3, blank=True, default="LON")
    location = models.CharField(max_length=3, blank=True, default="WIO")
    # software characteristics
    # runtimeimageversion=models.ForeignKey('SoftwareVersion', verbose_name="Version")
    # pricing=models.ForeignKey(PricingPolicy) # TODO / FIXME
    # current status
    available_distributions = models.ManyToManyField('compute.BaseDistribution', related_name="nodes")

    @staticmethod
    def invoke_localhost():
        rf = Node.objects.filter(hostname=socket.gethostname())
        if rf.count() != 0:
            return rf[0]
        rf = Node()
        rf.basetype = "we"
        rf.hosttype = "lxub-amd64-trusty"
        rf.started_at = datetime.datetime.now(
        ) - datetime.timedelta(0, float(uptime()[0]), 0)
        rf.update_based_on_localhost()
        if (hasattr(rf, "on_add")):
            rf.on_add(None)
        rf.save()
        return rf

    def update_based_on_localhost(self):
        self.host_ip = get_local_ip()
        self.cpu_count = get_cpu_count()
        self.system_load = os.getloadavg()
        mi = meminfo()
        self.total_memory = int(mi['MemTotal'][:-3])
        self.available_memory = int(mi['MemFree'][:-3])
        self.last_ping = datetime.datetime.now()
        self.available_disk = get_local_df()
        if (hasattr(self, "on_update")):
            self.on_update(None)
        self.save()

    def update_available_distributions(self):
        self.available_distributions.all().delete()
        from compute.models import BaseDistribution
        r = []
        if self.basetype == 'co':
            for cd in self.metadata.get("available_distributions", []):
                bd = BaseDistribution.invoke_by_name(cd)
                self.available_distributions.add(bd)
                r.append(cd + "#" + bd.id)
        return r

    @staticmethod
    def request_node_for_storage(req_mem=1):
        n = Node.objects.filter(
            basetype='co').order('-available_bucket_storage')[0]
        return n

    @staticmethod
    def get_available_compute_node(req_mem=1):
        n = Node.objects.filter(basetype='co')
        if n:
            return n[0]

    @staticmethod
    def get_storage(req_mem=1, basenode=None):
        # FIXME:  we shall convert this to a mongo api / transctional thing
        ba = None
        from cloud.models import BucketAlloc
        with transaction.atomic():
            if basenode is None:
                n = selg.request_node_for_storage(req_mem)
            else:
                n = basenode
            ba = BucketAlloc()
            n.remaining_free_hd -= req_mem
            ba.node = n
            ba.bucketid = uuid.uuid1()
            ba.required_memory = req_mem
            ba.save()
            n.save()
        return ba

    @staticmethod
    def can_add(request):
        return False

    def can_update(self, request):
        return False

    def __unicode__(self):
        return str(self.hostname) + "(" + str(self.hostip) + ")"

    def enumerate_compatible_distributions(self):
        from compute.models import BaseDistribution
        ## FIXME: Provide something better
        return BaseDistribution.objects.all()

    def start_ssh_server(self, user, dependencies=[], request_update=None, debug=False, basedistribution=None):
        from compute.models import SchedReqRep
        # if user.is_superuser:
        #    debug=True
        reqrep = SchedReqRep()
        #                reqrep.request={"opcode":"ssh#run/run","command":" [ -d /var/run/sshd ] || (echo creating /var/run/sshd;mkdir /var/run/sshd); ls -ld /var/run/sshd; /usr/sbin/sshd -D -e -p 1048",'sshkey':[k.public_key for k in user.sshkey_rev_owner.all() ],'dependencies':dependencies}
        reqrep.request = {"opcode": "ssh#run/run",
                          "command": " [ -d /var/run/sshd ] || mkdir /var/run/sshd;/usr/sbin/sshd -D -e -p 1048" + (
                          " -ddd" if debug else ""), 'sshkey': [k.public_key for k in user.sshkey_rev_owner.all()],
                          'dependencies': dependencies, 'basedistribution_id': basedistribution}
        if request_update != None:
            reqrep.request.update(request_update)
        reqrep.target_compute_node = self
        reqrep.ordered_by = user
        reqrep.on_add(None)
        reqrep.save()
        reqrep.send()
        return reqrep

    def send_reqrep(self, user, reqrepr):
        from compute.models import SchedReqRep
        reqrep = SchedReqRep()
        reqrep.request = reqrepr
        reqrep.target_compute_node = self
        reqrep.ordered_by = user
        reqrep.on_add(None)
        reqrep.save()
        return reqrep.send(), reqrep

    def workers_status(self):
        return self.send_reqrep(None, {'opcode': 'status'})

    class WIDEIO_Meta:
        NO_DRAFT = True
        DISABLE_VIEW_ACCOUNTING = True
        icon = "icon-circle"
        permissions = dec.perm_for_admin_only

        class Actions:
            @wideio_action(
                "Alloc Bucket",
                possible=lambda self,
                                request: request.user.is_superuser,
                mimetype="text/html")
            def request_bucket(self, request):
                return HttpResponseRedirect(
                    Node.get_storage(1, self).get_view_url())

            @wideio_action(possible=lambda self, request: request.user.is_staff, mimetype="text/html")
            def start_ssh_server(self, request):
                reqrep = self.start_ssh_server(request.user)
                return {'_redirect': reqrep.get_view_url()}

            @wideio_action(
                possible=lambda self,
                                request: request.user.is_staff,
                mimetype="text/html")
            def admin_ssh(self, request):
                login = request.user.first_name.lower()[0] + request.user.first_name.upper()[0]
                return {
                    '_redirect': "chrome-extension://pnhechapfaindjhompbnflcldabbghjo/html/nassh.html#" + login + "@" + self.hostname}

            @wideio_action(
                possible=lambda self, request: request.user.is_staff
            )
            def sync_available_dist(self, request):
                r = self.update_available_distributions()
                return 'alert("' + ",".join(r) + '");'

        MOCKS = {
            'default': [
                {
                    '@id': 'cloud-node-0000',
                    'hostname': 'node000',
                    'hostip': '10.10.10.1',
                    'basetype': 'we',
                    'hosttype': '',
                    'pub_key_ssh': '',
                    'cpu_count': 4,
                    'total_memory': 8 * 1024 * 1024 * 1024,
                    'available_memory': 4 * 1024 * 1024 * 1024,
                    'nworkers': 10,
                    'system_load': "(4,3,2)",
                    'started_at': datetime.datetime(2016, 10, 5),
                    'available_disk': 10 * 1024 * 1024 * 1024,
                    'available_bucket_storage': 20,
                    'last_ping': datetime.datetime(2015, 10, 10, 10, 10, 10),
                    'region': 'LON',
                    'location': 'WIO',
                    'available_distributions': []
                }
            ]
        }


@wideio_relaction("accounts.UserAccount",
                  possible=lambda o,r: (r.user == o),
                           icon="icon-terminal",
                           mimetype="text/html",
                           xattrs=" title=\"Start a new terminal\" ")
def open_terminal(self, request):
    from network.models import DirectMessage
    node=Node.get_available_compute_node()
    if node:
        req=node.start_ssh_server(request.user)
        return {'_redirect':req.get_view_url()}
    else:
        from django.contrib import messages
        messages.add_message(request, messages.WARNING, "No node to open")
        return {'_redirect': Node.get_list_url()}
